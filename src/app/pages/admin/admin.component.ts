import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminService } from 'src/app/shared/admin.service';
import { User } from 'src/app/shared/models/User';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['username', 'email', 'role', 'changeRole'];
  dataSource: MatTableDataSource<User> = new MatTableDataSource<User>();
  isButtonDisable: boolean = false;

  constructor(
    private adminService: AdminService
  ) { }

  ngOnInit(): void {
    this.adminService.getUsers()
      .subscribe((users: User[]) => {
        this.dataSource.data = users;
      })
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  changeRole(user: User) {
    this.adminService.changeRole(user.username).subscribe(() => {
      this.isButtonDisable = true;
      this.adminService.getUsers()
        .subscribe((users: User[]) => {
          this.dataSource.data = users;
          this.isButtonDisable = false;
        })
    });
  }
}
