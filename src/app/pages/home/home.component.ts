import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JoinGameDialogComponent } from 'src/app/join-game-dialog/join-game-dialog.component';
import { FantasyTeam } from 'src/app/shared/models/FantasyTeam';
import { TeamsService } from 'src/app/shared/teams.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  teams$!: Observable<FantasyTeam[]>;

  constructor(private teamsService: TeamsService, public dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    this.teams$ = this.teamsService.getMyTeams();
  }

  openJoinDialog() {
    const dialogRef = this.dialog.open(JoinGameDialogComponent);

    dialogRef.afterClosed().subscribe(fantasyGameId => {
      if (fantasyGameId) {
        localStorage.setItem("currentGame", fantasyGameId);
        this.router.navigateByUrl(`/team?gameId=${fantasyGameId}`);
      }
    });
  }

  goToTeam(team: FantasyTeam) {
    localStorage.setItem("currentGame", team.fantasyGameId.toString());
    this.router.navigateByUrl(`/team?gameId=${team.fantasyGameId}`);
  }
}
