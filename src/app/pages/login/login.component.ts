import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/auth.service';
import { Subject } from 'rxjs';
import { LoginRequest } from 'src/app/shared/models/LoginRequest';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginValid = true;
  public email = '';
  public password = '';
  private destroySub$ = new Subject<void>();

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.auth.isAuthenticated$.pipe(
      filter((isAuthenticated: boolean) => isAuthenticated),
      takeUntil(this.destroySub$)
    ).subscribe(_ => this.router.navigate(['/home']))
  }

  public onSubmit() {
    this.loginValid = true;
    this.auth.login({ email: this.email, password: this.password })
      .subscribe({
        next: _ => {
          this.loginValid = true;
          this.router.navigate(['/home'])
          window.location.reload();
        },
        error: err => {
          console.log(err);
          this.loginValid = false
        }
      })
  }
}
