import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { filter, Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm = new FormGroup({
    email: new FormControl(),
    username: new FormControl(),
    password: new FormControl(),
  });
  private destroySub$ = new Subject<void>();
  registerValid: boolean = true;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.auth.isAuthenticated$.pipe(
      filter((isAuthenticated: boolean) => isAuthenticated),
      takeUntil(this.destroySub$)
    ).subscribe(_ => this.router.navigate(['/home']))
  }

  get email(): string {
    return this.signupForm.get("email")?.value;
  }

  get password(): string {
    return this.signupForm.get("password")?.value;
  }

  get username(): string {
    return this.signupForm.get("username")?.value;
  }

  public onSubmit() {
    this.registerValid = true;
    this.auth.signup({ username: this.username, email: this.email, password: this.password })
      .subscribe({
        next: _ => {
          this.registerValid = true;
          this.router.navigate(['/home'])
          window.location.reload();
        },
        error: _ => this.registerValid = false
      })
  }

}
