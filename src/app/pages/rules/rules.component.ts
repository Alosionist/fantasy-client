import { Component, OnInit } from '@angular/core';
import { MatCheckboxDefaultOptions, MAT_CHECKBOX_DEFAULT_OPTIONS } from '@angular/material/checkbox';
import { MatTableDataSource } from '@angular/material/table';
import { FantasyGameService } from 'src/app/shared/fantasy-game.service';
import { FantasyGame } from 'src/app/shared/models/FantasyGame';
import { GWRules } from 'src/app/shared/models/GWRules';
import { PointsRule } from 'src/app/shared/models/PointsRule';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss'],
  providers: [
    { provide: MAT_CHECKBOX_DEFAULT_OPTIONS, useValue: { clickAction: 'noop' } as MatCheckboxDefaultOptions }
  ]
})
export class RulesComponent implements OnInit {
  displayedColumnsGwTable: string[] = ['gw', 'substitutions', 'budget', 'sameTeamPlayers'];
  displayedColumnsPointsTable: string[] = ['action', 'position', 'points'];
  fantasyGame: FantasyGame;
  gwRules: MatTableDataSource<GWRules>;
  pointsRules: MatTableDataSource<PointsRule>;

  constructor(private gameService: FantasyGameService) { }

  ngOnInit(): void {
    const fantasyGameId = parseInt(localStorage.getItem("currentGame") || "-1");

    this.gameService.getFantasyGame(fantasyGameId)
      .subscribe(game => {
        this.fantasyGame = game
        this.gwRules = new MatTableDataSource<GWRules>(game.gameWeekRules);
        this.pointsRules = new MatTableDataSource<PointsRule>(game.pointsRules);
      });
  }

}
