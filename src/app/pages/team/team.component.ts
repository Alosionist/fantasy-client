import { Component, OnInit } from '@angular/core';
import { GameWeekDataSummary } from 'src/app/shared/models/GameWeekDataSummary';
import { TeamsService } from 'src/app/shared/teams.service';
import { map, Observable, tap } from 'rxjs';
import { GameWeekData } from 'src/app/shared/models/GameWeekData';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { FantasyPlayer } from 'src/app/shared/models/FantasyPlayer';
import { RealDataService } from 'src/app/shared/real-data.service';
import { FantasyGameService } from 'src/app/shared/fantasy-game.service';
import { MatDialog } from '@angular/material/dialog';
import { PlayerStatsComponent } from 'src/app/player-stats/player-stats.component';
import { FantasyGame } from 'src/app/shared/models/FantasyGame';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  gameweeks$!: Observable<GameWeekDataSummary[]>;
  gameWeekData$: Observable<GameWeekData>;
  fantasyGameId: number;
  currentTabGw!: number;
  fantasyGame: FantasyGame;

  constructor(
    private teamsService: TeamsService,
    private gameService: FantasyGameService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private realDataService: RealDataService, 
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (params["gameId"]) {
          localStorage.setItem("currentGame", params["gameId"]);
        }

        this.fantasyGameId = parseInt(localStorage.getItem("currentGame") || "-1");
        this.gameService.getFantasyGame(this.fantasyGameId)
          .subscribe(fantasyGame => this.fantasyGame = fantasyGame);
        this.gameweeks$ = this.teamsService.getGameWeeks(this.fantasyGameId)
          .pipe(map((gameWeeks: GameWeekDataSummary[]) => {
            if (gameWeeks.length == 0) {
              this.router.navigateByUrl('/transfers');
            }
            return gameWeeks.sort((prev, current) => prev.gw > current.gw ? -1 : 1)
          }));
      })
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    const gw = parseInt(tabChangeEvent.tab.ariaLabel);
    this.currentTabGw = gw;
    this.gameWeekData$ = this.teamsService.getGameWeekLineup(this.fantasyGameId, gw);
  }

  open(player: FantasyPlayer) {
    const playerWithTotalPoints = this.fantasyGame.players.find(p => p.fantasyPlayerId == player.fantasyPlayerId)
    this.realDataService.getPlayerAllStats(player.playerId, this.fantasyGame.competitionId)
      .subscribe(playerStats => this.dialog.open(PlayerStatsComponent, { data: { playerStats, player: playerWithTotalPoints } }))
  }
}
