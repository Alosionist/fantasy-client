import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { FantasyGameService } from 'src/app/shared/fantasy-game.service';
import { FantasyTeam } from 'src/app/shared/models/FantasyTeam';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['rank', 'username', 'name', 'totalPoints'];
  dataSource: MatTableDataSource<FantasyTeam> = new MatTableDataSource<FantasyTeam>();
  teams: FantasyTeam[];
  gameName: string;

  constructor(
    private route: ActivatedRoute,
    private fantasyGameService: FantasyGameService
  ) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (params["gameId"]) {
          localStorage.setItem("currentGame", params["gameId"]);
        }

        const fantasyGameId = parseInt(localStorage.getItem("currentGame") || "-1");

        this.fantasyGameService.getFantasyGameTable(fantasyGameId)
          .subscribe((teams: FantasyTeam[]) => {
            this.gameName = teams[0].fantasyGameName;
            this.teams = teams.sort((a, b) => b.totalPoints - a.totalPoints)
            let i = 1;
            this.teams.forEach(team => {
              team.rank = i++;
            });
            this.dataSource.data = this.teams;
          })
      });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

}
