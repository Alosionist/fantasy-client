import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { FantasyGameService } from '../shared/fantasy-game.service';
import { FantasyGame } from '../shared/models/FantasyGame';
import { FantasyPlayer } from '../shared/models/FantasyPlayer';
import { GameWeekData } from '../shared/models/GameWeekData';
import { GWRules } from "../shared/models/GWRules";
import { TeamsService } from '../shared/teams.service';
import { MatSort } from '@angular/material/sort';
import { SubsRequest } from '../shared/models/SubsRequest';
import { PlayerSelection } from '../shared/models/PlayerSelection';
import { Position } from '../shared/models/Position';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';
import { FantasyTeam } from '../shared/models/FantasyTeam';
import { PlayerStatsComponent } from '../player-stats/player-stats.component';
import { RealDataService } from '../shared/real-data.service';
import { Router } from '@angular/router';

interface Team {
  name: string;
  id: number;
}

@Component({
  selector: 'app-transfers',
  templateUrl: './transfers.component.html',
  styleUrls: ['./transfers.component.scss']
})
export class TransfersComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['select', 'name', 'team', 'price', 'points'];
  gameWeekData: GameWeekData;
  originalPlayers: number[] = [];
  fantasyGame: FantasyGame;
  fantasyGameId: number;
  dataSource: MatTableDataSource<FantasyPlayer> = new MatTableDataSource<FantasyPlayer>();
  filterPosition: Position = Position.DEFENDER;
  filterTeamId: number;
  fantasyTeam: FantasyTeam;
  totalPrice: number;
  subsMadeBefore: number = 0;
  subsMade: number = 0;
  gwRules: GWRules;
  teams: Team[] = [];

  constructor(
    private router: Router,
    private teamsService: TeamsService,
    private fantasyGameService: FantasyGameService,
    public dialog: MatDialog,
    private realDataService: RealDataService
  ) { }

  ngOnInit(): void {
    this.fantasyGameId = parseInt(localStorage.getItem("currentGame") || "-1");
    this.teamsService.getCurrentLineup(this.fantasyGameId)
      .subscribe((gameWeekData: GameWeekData) => {
        this.gameWeekData = gameWeekData;
        this.subsMadeBefore = gameWeekData.subsMade;
        this.subsMade = this.subsMadeBefore;
        this.originalPlayers = [...gameWeekData.lineup, ...gameWeekData.bench].map(p => p.fantasyPlayerId);
        this.teamsService.getMyTeam(gameWeekData.fantasyTeamId).subscribe((fantasyTeam: FantasyTeam) => this.fantasyTeam = fantasyTeam);

        this.fantasyGameService.getFantasyGame(this.fantasyGameId)
          .subscribe((fantasyGame: FantasyGame) => {
            this.fantasyGame = fantasyGame;
            this.gwRules = fantasyGame.gameWeekRules.filter(r => gameWeekData.gw >= r.gameWeek).reduce((prev, current) => (prev.gameWeek > current.gameWeek) ? prev : current);
            this.dataSource.data = fantasyGame.players;
            this.gameWeekData.lineup = this.dataSource.data
              .filter(row => !!this.findInLineup(row))
              .map(player => { player.selected = true; return player });
            this.gameWeekData.bench = this.dataSource.data
              .filter(row => !!this.findInBench(row))
              .map(player => { player.selected = true; return player });
            this.totalPrice = [...this.gameWeekData.lineup, ...this.gameWeekData.bench].map(p => p.price).reduce((a, b) => a + b, 0);
            fantasyGame.players.forEach((p: FantasyPlayer) => {
              const team = { id: p.teamId, name: p.team } as Team;
              if (!this.teams.find(t => t.id == team.id)) {
                this.teams.push(team);
              }
            })
            this.applyFilter();
          });
      });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  filterPos(position: string) {
    this.filterPosition = position as Position;
    this.applyFilter();
  }

  applyFilter() {
    this.dataSource.data = this.fantasyGame.players
      .filter(p => !this.filterPosition || p.position === this.filterPosition)
      .filter(p => !this.filterTeamId || p.teamId === this.filterTeamId);
  }

  remove(player: FantasyPlayer) {
    this.select(player)
  }

  open(player: FantasyPlayer) {
    this.realDataService.getPlayerAllStats(player.playerId, this.fantasyGame.competitionId)
      .subscribe(playerStats => this.dialog.open(PlayerStatsComponent, { data: { playerStats, player } }))
  }

  select(row: FantasyPlayer) {
    if (row.selected) { // remove if selected
      if (this.findInLineup(row)) {
        this.gameWeekData.lineup = this.gameWeekData.lineup.filter(p => p.fantasyPlayerId != row.fantasyPlayerId)
      } else if (this.findInBench(row)) {
        this.gameWeekData.bench = this.gameWeekData.bench.filter(p => p.fantasyPlayerId != row.fantasyPlayerId)
      }
      row.selected = false;
      this.totalPrice = [...this.gameWeekData.lineup, ...this.gameWeekData.bench].map(p => p.price).reduce((a, b) => a + b, 0);
    } else { // add if not selected
      this.addPlayer(row);
    }
  }

  addPlayer(player: FantasyPlayer) {
    const tempLineup = [...this.gameWeekData.lineup];
    const tempBench = [...this.gameWeekData.bench];

    if (tempLineup.length < 11) { // add to lineup if possible
      tempLineup.push(player);
    } else if (tempBench.length < 4) { // add to bench if possible
      tempBench.push(player);
    } else {
      this.validationFailedPopup(player, null)
      return;
    }

    const subsRequest: SubsRequest = this.createSubsRequst(tempLineup, tempBench);

    this.teamsService.validateSubs(subsRequest)
      .subscribe({
        next: () => {
          player.selected = true
          this.gameWeekData.lineup = [...tempLineup]
          this.gameWeekData.bench = [...tempBench]
          this.totalPrice = [...this.gameWeekData.lineup, ...this.gameWeekData.bench].map(p => p.price).reduce((a, b) => a + b, 0);
          const currPlayers = [...this.gameWeekData.lineup, ...this.gameWeekData.bench].map(p => p.fantasyPlayerId);
          this.subsMade = this.subsMadeBefore + currPlayers.filter(p => !this.originalPlayers.includes(p) && this.originalPlayers.length > 0).length;
        }, error: (message) => this.validationFailedPopup(player, message.error)
      })
  }

  validationFailedPopup(player: FantasyPlayer, message: any): void {
    player.selected = false;
    this.dialog.open(MessageDialogComponent, { data: message })
  }

  createSubsRequst(lineup: FantasyPlayer[], bench: FantasyPlayer[]): SubsRequest {
    const req = {
      fantasyGameId: this.fantasyGameId,
      bench: bench.map(p => {
        return { captain: false, id: p.fantasyPlayerId } as PlayerSelection
      }),
      starting: lineup.map(p => {
        return { captain: p.fantasyPlayerId == this.gameWeekData.captainId, id: p.fantasyPlayerId } as PlayerSelection
      }),
      chips: { benchBoost: this.gameWeekData.benchBoostActive, tripleCaption: this.gameWeekData.tripleCaptionActive, wildcard: this.gameWeekData.wildcardActive }
    } as SubsRequest;

    return req;
  }

  makeSubs() {
    this.teamsService.makeSubs(this.createSubsRequst(this.gameWeekData.lineup, this.gameWeekData.bench))
      .subscribe({
        next: () => this.router.navigate(['/team']),
        error: (message) => this.dialog.open(MessageDialogComponent, { data: message.error })
      });
  }

  findInLineup(player: FantasyPlayer): FantasyPlayer {
    return this.gameWeekData.lineup.find(p => p.fantasyPlayerId === player.fantasyPlayerId)
  }

  findInBench(player: FantasyPlayer): FantasyPlayer {
    return this.gameWeekData.bench.find(p => p.fantasyPlayerId === player.fantasyPlayerId)
  }
}
