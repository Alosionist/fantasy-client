import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map, Observable, of } from 'rxjs';
import { AuthResponse } from './models/AuthResponse';
import { LoginRequest } from './models/LoginRequest';
import { RegisterRequest } from './models/RegisterRequest';
import { Role } from './models/Role';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  private readonly LOGIN_PATH = "/api/auth/login";
  private readonly SIGNUP_PATH = "/api/auth/register";
  private authSubject$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
    this.authSubject$.next(this.isAuthenticated())
  }

  public get isAuthenticated$(): Observable<boolean> {
    return this.authSubject$.asObservable();
  }

  public get role(): Role {
    return localStorage.getItem("role") as Role;
  }

  public get username(): string {
    return localStorage.getItem("username") as string;
  }

  public get token(): string {
    return localStorage.getItem("token") as string;
  }

  public get expireTime(): string {
    return localStorage.getItem("expireTime") as string;
  }

  public ngOnDestroy(): void {
    this.authSubject$.next(false);
    this.authSubject$.complete();
  }

  public signup(registerRequest: RegisterRequest) {
    return this.http.post<AuthResponse>(this.SIGNUP_PATH, registerRequest).pipe(
      map((response: AuthResponse) => {
        localStorage.setItem("token", response.token);
        localStorage.setItem("expireTime", response.expireDate);
        localStorage.setItem("role", response.role)
        localStorage.setItem("username", response.username)
        this.authSubject$.next(true)
      })
    )
  }

  public login(loginRequest: LoginRequest): Observable<void> {
    return this.http.post<AuthResponse>(this.LOGIN_PATH, loginRequest).pipe(
      map((response: AuthResponse) => {
        localStorage.setItem("token", response.token);
        localStorage.setItem("expireTime", response.expireDate);
        localStorage.setItem("role", response.role)
        localStorage.setItem("username", response.username)
        this.authSubject$.next(true)
      })
    )
  }

  public logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("expireTime");
    localStorage.removeItem("role");
    localStorage.removeItem("username");
    localStorage.removeItem("currentGame");
    window.location.reload();
  }
  private isAuthenticated(): boolean {
    return !!localStorage.getItem("token") && new Date(this.expireTime).getTime() > Date.now();
  }

  public getHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    })
  }
}

