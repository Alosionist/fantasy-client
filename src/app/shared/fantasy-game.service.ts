import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { FantasyGame } from './models/FantasyGame';
import { FantasyTeam } from './models/FantasyTeam';

@Injectable({
  providedIn: 'root'
})
export class FantasyGameService {

  private readonly GAMES_PATH = "/api/fantasy/games";

  constructor(private http: HttpClient, private auth: AuthService) { }

  public getFantasyGames(): Observable<FantasyGame[]> {
    return this.http.get<FantasyGame[]>(this.GAMES_PATH, { headers: this.auth.getHeaders() });
  }

  public getFantasyGame(fantasyGameId: number): Observable<FantasyGame> {
    return this.http.get<FantasyGame>(`${this.GAMES_PATH}/${fantasyGameId}`, { headers: this.auth.getHeaders() });
  }

  public getFantasyGameTable(fantasyGameId: number): Observable<FantasyTeam[]> {
    return this.http.get<FantasyTeam[]>(`${this.GAMES_PATH}/${fantasyGameId}/table`, { headers: this.auth.getHeaders() });
  }

  public getFantasyGamesByCompetittionId(competitionId: string): Observable<FantasyGame[]> {
    return this.http.get<FantasyGame[]>(`${this.GAMES_PATH}?competitionId=${competitionId}`, { headers: this.auth.getHeaders() });
  }
}
