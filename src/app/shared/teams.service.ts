import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { CreateTeamRequest } from './models/CreateTeamRequest';
import { GameWeekData } from './models/GameWeekData';
import { FantasyTeam } from './models/FantasyTeam';
import { GameWeekDataSummary } from './models/GameWeekDataSummary';
import { SubsRequest } from './models/SubsRequest';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
  private readonly TEAMS_PATH = "/api/fantasy/teams";

  constructor(private http: HttpClient, private auth: AuthService) { }

  public getMyTeams(): Observable<FantasyTeam[]> {
    return this.http.get<FantasyTeam[]>(this.TEAMS_PATH, {headers: this.auth.getHeaders()});
  }

  public getMyTeam(fantasyTeamId: number): Observable<FantasyTeam> {
    return this.http.get<FantasyTeam>(this.TEAMS_PATH + '/' + fantasyTeamId, { headers: this.auth.getHeaders() });
  }

  public createNewTeam(createTeamRequest: CreateTeamRequest): Observable<void> {
    return this.http.post<void>(this.TEAMS_PATH, createTeamRequest, { headers: this.auth.getHeaders()});
  }

  public getGameWeeks(fantasyGameId: number): Observable<GameWeekDataSummary[]> {
    return this.http.get<GameWeekDataSummary[]>(`${this.TEAMS_PATH}/gw-points?fantasyGameId=${fantasyGameId}`, { headers: this.auth.getHeaders() });
  }

  public getGameWeekLineup(fantasyGameId: number, gw: number): Observable<GameWeekData> {
    return this.http.get<GameWeekData>(`${this.TEAMS_PATH}/lineup?fantasyGameId=${fantasyGameId}&gw=${gw}`, { headers: this.auth.getHeaders() });
  }

  public getCurrentLineup(fantasyGameId: number): Observable<GameWeekData> {
    return this.http.get<GameWeekData>(`${this.TEAMS_PATH}/lineup?fantasyGameId=${fantasyGameId}`, { headers: this.auth.getHeaders() });
  }

  public validateSubs(subsRequest: SubsRequest): Observable<void> {
    return this.http.post<void>(this.TEAMS_PATH + "/validateSubs", subsRequest, { headers: this.auth.getHeaders() });
  }

  public makeSubs(subsRequest: SubsRequest): Observable<void> {
    return this.http.post<void>(this.TEAMS_PATH + "/subs", subsRequest, { headers: this.auth.getHeaders() });
  }

}
