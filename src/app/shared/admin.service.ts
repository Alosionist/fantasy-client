import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { User } from './models/User';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private readonly USERS_PATH = "/api/admin/users";

  constructor(private http: HttpClient, private auth: AuthService) { }

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.USERS_PATH, { headers: this.auth.getHeaders() });
  }

  public changeRole(username: string): Observable<void> {
    return this.http.post<void>(this.USERS_PATH + "/" + username,"", { headers: this.auth.getHeaders() });
  }
}
