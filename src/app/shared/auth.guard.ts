import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, map } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.isAuthenticated$
      .pipe(map((isAuthenticated: boolean) => {
        if (isAuthenticated) {
          if (this.checkRole(route)) {
            return true;
          } else {
            this.router.navigate(["/home"])
            return false;
          }
        }

        this.router.navigate(["/login"])
        return false;
      }));
  }

  checkRole(route: ActivatedRouteSnapshot): boolean {
    const userRole = this.auth.role;

    if (route.data['roles'] && route.data['roles'].indexOf(userRole) === -1) {
      return false;
    }

    return true;
  }
}
