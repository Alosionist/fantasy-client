import { Role } from "./Role";

export interface AuthResponse {
  username: string;
  token: string;
  expireDate: string;
  role: Role
}