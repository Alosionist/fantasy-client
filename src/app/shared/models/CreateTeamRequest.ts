export interface CreateTeamRequest {
  name: string;
  fantasyGameId: number;
}