import { Chips } from "./Chips";
import { PlayerSelection } from "./PlayerSelection";

export interface SubsRequest {
 fantasyGameId: number;
 starting: PlayerSelection[];
 bench: PlayerSelection[];
 chips: Chips;
}