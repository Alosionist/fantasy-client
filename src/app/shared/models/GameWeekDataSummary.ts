export interface GameWeekDataSummary {
  gw: number;
  points: number;
}