import { FantasyPlayer } from "./FantasyPlayer";

export interface GameWeekData {
    fantasyTeamId: number;
    gw: number;
    points: number;
    subsMade: number;
    captainId: number;
    wildcardActive: boolean;
    tripleCaptionActive: boolean;
    benchBoostActive: boolean;
    lineup: Array<FantasyPlayer>;
    bench: Array<FantasyPlayer>;
}