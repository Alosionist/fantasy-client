export interface GWRules {
  gameWeek: number;
  substitutions: number;
  budget: number;
  sameTeamPlayers: number;
}