export interface PlayerSelection {
  id: number;
  captain: boolean;
}