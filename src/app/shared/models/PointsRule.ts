import { Action } from "./Action";
import { Position } from "./Position";

export interface PointsRule {
  action: Action;
  position: Position;
  points: number;
}