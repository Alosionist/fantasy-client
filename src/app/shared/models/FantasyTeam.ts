export interface FantasyTeam {
  id: number;
  rank: number;
  name: string;
  username: string;
  totalPoints: number;
  fantasyGameId: number;
  fantasyGameName: string;
  competitionName: string;
  wildcardAvailable: boolean;
  tripleCaptionAvailable: boolean;
  benchBoostAvailable: boolean;
}