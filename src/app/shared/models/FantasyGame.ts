import { Chips } from "./Chips";
import { FantasyPlayer } from "./FantasyPlayer";
import { GWRules } from "./GWRules";
import { PointsRule } from "./PointsRule";

export interface FantasyGame {
  id: number;
  name: string;
  competitionId: number;
  competition: string;
  bench: boolean;
  chips: Chips;
  gameWeekRules: GWRules[];
  pointsRules: PointsRule[];
  players: FantasyPlayer[];
}