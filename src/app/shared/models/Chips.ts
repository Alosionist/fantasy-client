export interface Chips {
  wildcard: boolean;
  tripleCaption: boolean;
  benchBoost: boolean;
}