export interface PlayerStatsId {
  leagueId: number;
  playerId: number;
  gameWeek: number;
}