export interface CompetitionInfo {
  id: number;
  name: string;
}