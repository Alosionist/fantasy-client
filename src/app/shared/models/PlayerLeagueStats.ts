export interface PlayerLeagueStats {
  leagueId: number;
  playerId: number;
  minutes: number;
  appearances: number;
  appearancesOver60: number;
  goals: number;
  saves: number;
  assists: number;
  ownGoals: number;
  cleanSheets: number;
  penaltiesWon: number;
  penaltiesCaused: number;
  penaltiesSaved: number;
  penaltiesMissed: number;
  yellowCards: number;
  redCards: number;
  mvps: number;
}
