import { PlayerStatsId } from "./PlayerStatsId";

export interface PlayerStats {
  id: PlayerStatsId;
  minutes: number;
  goals: number;
  saves: number;
  assists: number;
  ownGoals: number;
  goalsConceded: number;
  penaltiesWon: number;
  penaltiesCaused: number;
  penaltiesSaved: number;
  penaltiesMissed: number;
  yellowCards: number;
  redCard: boolean;
  mvp: boolean;
}