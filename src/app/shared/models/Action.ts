export enum Action {
  APPEARANCE = 'appearance',
  APPEARANCE_OVER_60 = 'appearanceOver60',
  SAVE = 'save',
  GOAL = 'goal',
  ASSIST = 'assist',
  OWN_GOAL = 'ownGoal',
  GOAL_CONCEDED = 'goalConceded',
  CLEAN_SHEET = 'cleanSheets',
  PENALTY_WON = 'penaltiesWon',
  PENALTY_CAUSED = 'penaltiesCaused',
  PENALTY_SAVED = 'penaltiesSaved',
  PENALTY_MISSED = 'penaltiesMissed',
  YELLOW_CARD = 'yellowCard',
  RED_CARD = 'redCard',
  MVP = 'mvp'
}