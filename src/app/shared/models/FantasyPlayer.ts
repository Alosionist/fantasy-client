import { Position } from "./Position";

export interface FantasyPlayer {
  playerId: number;
  fantasyPlayerId: number;
  teamId: number;
  team: string;
  name: string;
  price: number;
  points: number;
  position: Position;
  selected: boolean;
}