import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { CompetitionInfo } from './models/CompetitionInfo';
import { PlayerLeagueStats } from './models/PlayerLeagueStats';
import { PlayerStats } from './models/PlayerStats';

@Injectable({
  providedIn: 'root'
})
export class RealDataService {
  private readonly COMPETITION_PATH = "/real-data/competitions/names";
  private readonly PLAYER_PATH = "/real-data/players";

  constructor(private http: HttpClient, private auth: AuthService) { }

  public getCompetitions(): Observable<CompetitionInfo[]> {
    return this.http.get<CompetitionInfo[]>(this.COMPETITION_PATH, { headers: this.auth.getHeaders() });
  }

  public getPlayerStats(playerId: number, leagueId: number, gw: number): Observable<PlayerStats> {
    return this.http.get<PlayerStats>(`${this.PLAYER_PATH}/${playerId}/stats?leagueId=${leagueId}&gw=${gw}`, { headers: this.auth.getHeaders() });
  }

  public getPlayerAllStats(playerId: number, leagueId: number): Observable<PlayerLeagueStats> {
    return this.http.get<PlayerLeagueStats>(`${this.PLAYER_PATH}/${playerId}/AllStats?leagueId=${leagueId}`, { headers: this.auth.getHeaders() });
  }
}
