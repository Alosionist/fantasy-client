import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FantasyPlayer } from '../shared/models/FantasyPlayer';
import { PlayerLeagueStats } from '../shared/models/PlayerLeagueStats';

interface DialogData {
  playerStats: PlayerLeagueStats
  player: FantasyPlayer;
}

@Component({
  selector: 'app-player-stats',
  templateUrl: './player-stats.component.html',
  styleUrls: ['./player-stats.component.scss']
})
export class PlayerStatsComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public dialogData: DialogData) { }

  ngOnInit(): void {    

  }
}
