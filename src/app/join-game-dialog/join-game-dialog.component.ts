import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { FantasyGameService } from '../shared/fantasy-game.service';
import { CompetitionInfo } from '../shared/models/CompetitionInfo';
import { FantasyGame } from '../shared/models/FantasyGame';
import { RealDataService } from '../shared/real-data.service';
import { FormGroup, FormControl } from '@angular/forms';
import { TeamsService } from '../shared/teams.service';

@Component({
  selector: 'app-join-game-dialog',
  templateUrl: './join-game-dialog.component.html',
  styleUrls: ['./join-game-dialog.component.scss']
})
export class JoinGameDialogComponent implements OnInit {
  resultValid = true;
  message = '';
  competitions$!: Observable<CompetitionInfo[]>;
  games$!: Observable<FantasyGame[]>;
  joinForm = new FormGroup({
    competition: new FormControl(),
    game: new FormControl(),
    name: new FormControl(),
  });

  get competitionId(): string {
    return this.joinForm.get('competition')?.value;
  }

  get gameId(): string {
    return this.joinForm.get('game')?.value;
  }

  get name(): string {
    return this.joinForm.get('name')?.value;
  }

  constructor(
    public dialogRef: MatDialogRef<JoinGameDialogComponent>,
    private realDataService: RealDataService,
    private teamsService: TeamsService,
    private fantasyGameService: FantasyGameService
  ) { }

  ngOnInit(): void {
    this.competitions$ = this.realDataService.getCompetitions();
  }

  onCompetitionChange() {
    console.log(this.competitionId);

    this.games$ = this.fantasyGameService.getFantasyGamesByCompetittionId(this.competitionId);
  }

  onFormSubmit() {
    const id: number = parseInt(this.gameId);

    if (id && this.name) {
      this.teamsService.createNewTeam({ name: this.name, fantasyGameId: id })
        .subscribe({
          next: _ => this.dialogRef.close(this.gameId),
          error: err => {
            this.resultValid = false;            
            this.message = err.error.message;
          }
        });
      return true;
    } else {
      this.resultValid = false;
      this.message = "Choose competition and fantasy game";
    }

    return false;
  }

}
