import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FantasyPlayer } from '../shared/models/FantasyPlayer';

@Component({
  selector: 'app-player-box',
  templateUrl: './player-box.component.html',
  styleUrls: ['./player-box.component.scss']
})
export class PlayerBoxComponent implements OnInit {
  @Input() player!: FantasyPlayer;
  @Input() isEdit: boolean = false;
  @Input() isCaptain: boolean = false;
  @Output() removePlayer: EventEmitter<FantasyPlayer> = new EventEmitter();
  @Output() open: EventEmitter<FantasyPlayer> = new EventEmitter();
  data: string = "";


  constructor() { }

  ngOnInit(): void {
    if (this.isEdit) {
      this.data = this.player.price + ' M';
    } else {
      this.data = this.player.points + ' pts.';
    }
  }

  remove() {        
    this.removePlayer.emit(this.player);
  }

  click() {
    this.open.emit(this.player);
  }
}
