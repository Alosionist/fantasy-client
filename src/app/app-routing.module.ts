import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { AdminComponent } from './pages/admin/admin.component';
import { HelpComponent } from './pages/help/help.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RulesComponent } from './pages/rules/rules.component';
import { SignupComponent } from './pages/signup/signup.component';
import { TableComponent } from './pages/table/table.component';
import { TeamComponent } from './pages/team/team.component';
import { AuthGuard } from './shared/auth.guard';
import { Role } from './shared/models/Role';
import { TransfersComponent } from './transfers/transfers.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home',
  },
  {
    path: 'home',
    component: HomeComponent, canActivate: [AuthGuard],
    data: { roles: [Role.USER, Role.ADMIN, Role.SUPER]}
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'signup',
    component: SignupComponent,
  },
  {
    path: 'team',
    component: TeamComponent, canActivate: [AuthGuard],
    data: { roles: [Role.USER, Role.ADMIN, Role.SUPER] }
  },
  {
    path: 'transfers',
    component: TransfersComponent, canActivate: [AuthGuard],
    data: { roles: [Role.USER, Role.ADMIN, Role.SUPER] }
  },
  {
    path: 'rules',
    component: RulesComponent, canActivate: [AuthGuard],
    data: { roles: [Role.USER, Role.ADMIN, Role.SUPER] }
  },
  {
    path: 'table',
    component: TableComponent, canActivate: [AuthGuard],
    data: { roles: [Role.USER, Role.ADMIN, Role.SUPER] }
  },
  {
    path: 'users',
    component: AdminComponent, canActivate: [AuthGuard],
    data: { roles: [Role.SUPER] }
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
