import { Component, OnInit, OnChanges, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FantasyPlayer } from '../shared/models/FantasyPlayer';
import { GameWeekData } from '../shared/models/GameWeekData';
import { Position } from '../shared/models/Position';

@Component({
  selector: 'app-pitch',
  templateUrl: './pitch.component.html',
  styleUrls: ['./pitch.component.scss']
})
export class PitchComponent implements OnChanges {
  @Input() lineup: Array<FantasyPlayer> = [];
  @Input() bench: Array<FantasyPlayer> = [];
  @Input() isEdit: boolean = false;
  @Input() captainId: number = 0;
  @Output() remove: EventEmitter<FantasyPlayer> = new EventEmitter();
  @Output() open: EventEmitter<FantasyPlayer> = new EventEmitter();

  gk: Array<FantasyPlayer> = [];
  def: Array<FantasyPlayer> = [];
  mid: Array<FantasyPlayer> = [];
  fw: Array<FantasyPlayer> = [];

  ngOnChanges(): void {    
    this.gk = this.lineup.filter(player => player.position === Position.GOALKEEPER);
    this.def = this.lineup.filter(player => player.position === Position.DEFENDER);
    this.mid = this.lineup.filter(player => player.position === Position.MIDFIELDER);
    this.fw = this.lineup.filter(player => player.position === Position.FORWARD);
  }

  removePlayer(player: FantasyPlayer) {
    this.remove.emit(player)
  }

  click(player: FantasyPlayer) {
    this.open.emit(player)
  }
}
