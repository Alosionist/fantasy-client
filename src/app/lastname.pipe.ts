import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lastname'
})
export class LastnamePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    const parts = value.split(' ');
    return parts[parts.length - 1];
  }

}
