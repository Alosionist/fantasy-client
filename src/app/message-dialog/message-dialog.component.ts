import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ErrorMessage {
  message: string;
  status: number;
}

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent implements OnInit {
  private readonly SUBS_ERROR = "invalid subs number";
  private readonly SQUAD_ERROR = "invalid squad";
  private readonly LINEUP_ERROR = "invalid lineup";
  private readonly BUDGET_ERROR = "not enough budget";
  private readonly CAPTAIN_ERROR = "invalid caption id";
  private readonly SAME_TEAM_ERROR = "players on the same team"
  private readonly SQUAD_MESSAGE = "invalid squad - squad should have 2 gk, 5 def, 5 mid, 3 fw";
  private readonly LINEUP_MESSAGE = "invalid lineup - lineup should have 1 gk, 3-5 def, 3-5 mid, 1-3 fw";
  private readonly CAPTAIN_MESSAGE = "Please choose captain";
  private readonly SAME_TEAM_MESSAGE = "Invalid amount of players on the same team";

  message: string;

  constructor(@Inject(MAT_DIALOG_DATA) public errorMessage: ErrorMessage) { }

  ngOnInit(): void {
    if (this.errorMessage) {
      if (this.errorMessage.message.includes(this.SUBS_ERROR)) this.message = this.SUBS_ERROR;
      if (this.errorMessage.message.includes(this.SQUAD_ERROR)) this.message = this.SQUAD_MESSAGE;
      if (this.errorMessage.message.includes(this.LINEUP_ERROR)) this.message = this.LINEUP_MESSAGE;
      if (this.errorMessage.message.includes(this.BUDGET_ERROR)) this.message = this.BUDGET_ERROR;
      if (this.errorMessage.message.includes(this.CAPTAIN_ERROR)) this.message = this.CAPTAIN_MESSAGE;
      if (this.errorMessage.message.includes(this.SAME_TEAM_ERROR)) this.message = this.SAME_TEAM_MESSAGE; 
    } else {
      this.message = "invalid number of players"
    }
  }
}
